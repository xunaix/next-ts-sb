## install

```bash
yarn install
```

## development

```bash
yarn dev
```

## storybook

```bash
yarn storybook
```

# links

- [app](https://next-ts-sb.vercel.app)
- [storybook](https://next-ts-sb-storybook.vercel.app)
